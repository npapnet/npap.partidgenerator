﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace npap.PartIDGenerator.frms
{

    public partial class DataUserControl : UserControl
    {
        StringBuilder sb;
        #region Initialisation********************************************

        public DataUserControl()
        {
            InitializeComponent();
            sb = new StringBuilder();
            //Initialize Comboboxes
            cmbxType.DataSource = new BindingSource(npap.PartIDGenerator.Model.GlobalConstants.dictType, null);
            Dict2ComboBox(npap.PartIDGenerator.Model.GlobalConstants.dictGroupProcess, cmbxGroupProcess);
            //cmbxMachiningProcess1.DataSource = new BindingSource(npap.PartIDGenerator.Model.GlobalConstants.dictMachiningProcess, "Key");
            Dict2ComboBox(npap.PartIDGenerator.Model.GlobalConstants.dictMachiningProcess, cmbxMachiningProcess1);
            //cmbxMachiningProcess2.DataSource = new BindingSource(dictMachiningProcess, null);
            Dict2ComboBox(npap.PartIDGenerator.Model.GlobalConstants.dictMachiningProcess, cmbxMachiningProcess2);
            Dict2ComboBox(npap.PartIDGenerator.Model.GlobalConstants.dictFinishingProcess, cmbxFinishing);
            Dict2ComboBox(npap.PartIDGenerator.Model.GlobalConstants.dictRawType, cmbxRawForm);
        }

        private void DataUserControl_Load(object sender, EventArgs e)
        {


        }
        
        #endregion
        #region Auxilliary Methods********************************************

        /// <summary>
        /// Auxilliary method for ComboBox Initialisation
        /// </summary>
        /// <param name="myDict"></param>
        /// <param name="myCmbx"></param>
        private void Dict2ComboBox(Dictionary<string, int> myDict, ComboBox myCmbx)
        {
            myCmbx.DataSource = new BindingSource(myDict, null);
        }


        /// <summary>
        /// Returns the Key of the Dictionary<string, int> for the current combobox selection
        /// </summary>
        /// <param name="myCmbx"></param>
        /// <returns></returns>
        private string getDictionaryKey(ComboBox myCmbx)
        {
            var originalDictionary = ((Dictionary<string, int>)((BindingSource)myCmbx.DataSource).DataSource);
            return (originalDictionary.Keys.ElementAt(myCmbx.SelectedIndex));
        }
        /// <summary>
        /// Returns the Value of the Dictionary<string, int> for the current combobox selection
        /// </summary>
        /// <param name="myCmbx"></param>
        /// <returns></returns>
        private string getDictionaryValue(ComboBox myCmbx)
        {
            var originalDictionary = ((Dictionary<string, int>)((BindingSource)myCmbx.DataSource).DataSource);
            int randomValue = (int)(originalDictionary.Values.ElementAt(myCmbx.SelectedIndex));
            return randomValue.ToString("00");
        }
        /// <summary>
        /// Returns the Value of the Dictionary<string, string> for the current combobox selection
        /// </summary>
        /// <param name="myCmbx"></param>
        /// <returns></returns>

        private string getDictionaryStringValue(ComboBox myCmbx)
        {
            var originalDictionary = ((Dictionary<string, char>)((BindingSource)myCmbx.DataSource).DataSource);
            char randomValue = (char)(originalDictionary.Values.ElementAt(myCmbx.SelectedIndex));
            return randomValue.ToString();
        }
        #endregion

        /// <summary>
        /// Refreshes Box 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateTB(object sender, EventArgs e)
        {
            this.updateTB();
        }

        private void updateTB()
        {
            sb.Clear();
            try
            {
                sb.Append(getDictionaryStringValue(cmbxType));
                sb.Append(getDictionaryValue(cmbxGroupProcess));
                sb.Append(getDictionaryValue(cmbxMachiningProcess1));
                sb.Append(getDictionaryValue(cmbxMachiningProcess2));
                sb.Append(getDictionaryValue(cmbxFinishing));
                sb.Append(getDictionaryValue(cmbxRawForm));
            }
            catch (Exception)
            {

            }
            this.tbSortingCode.Text = sb.ToString();
        }

        #region GetMethods ***************************************************
        /// <summary>
        /// Returns the sort code of the Data User Control
        /// </summary>
        /// <returns></returns>
        internal string getSortCode()
        {
            return tbSortingCode.Text;
        }

        internal string getType()
        {
            return cmbxType.SelectedText;
        }

        internal string getGroupProcess()
        {
            return getDictionaryKey(cmbxGroupProcess);
        }

        internal string getMachiningProcess1()
        {
            return getDictionaryKey(cmbxMachiningProcess1);
        }

        internal string getMachiningProcess2()
        {
            return getDictionaryKey(cmbxMachiningProcess2);
        }

        internal string getFinishing()
        {
            return getDictionaryKey(cmbxFinishing);
        }

        internal string getRawForm()
        {
            return getDictionaryKey(cmbxRawForm);
        }
        internal string getDescription()
        {
            return tbDescription.Text;
        }
        
        #endregion

        internal void UpdateDUC(DataUserControl dataUserControl)
        {
            this.cmbxType.SelectedIndex = dataUserControl.cmbxType.SelectedIndex;
            this.cmbxGroupProcess.SelectedIndex = dataUserControl.cmbxGroupProcess.SelectedIndex;
            this.cmbxMachiningProcess1.SelectedIndex = dataUserControl.cmbxMachiningProcess1.SelectedIndex;
            this.cmbxMachiningProcess2.SelectedIndex = dataUserControl.cmbxMachiningProcess2.SelectedIndex;
            this.cmbxFinishing.SelectedIndex = dataUserControl.cmbxFinishing.SelectedIndex;
            this.cmbxRawForm.SelectedIndex = dataUserControl.cmbxRawForm.SelectedIndex;
            this.tbDescription.Text = dataUserControl.tbDescription.Text; 
            this.Refresh();
            this.updateTB();
        }

        internal void UpdateDUC(int TypeIndex, int GroupIndex, int Machining1Index, int Manhining2Index,
            int FinishingIndex, int RawFormIndex, string Description)
        {
            this.cmbxType.SelectedIndex = TypeIndex;
            this.cmbxGroupProcess.SelectedIndex = GroupIndex;
            this.cmbxMachiningProcess1.SelectedIndex = Machining1Index;
            this.cmbxMachiningProcess2.SelectedIndex = Manhining2Index;
            this.cmbxFinishing.SelectedIndex = FinishingIndex;
            this.cmbxRawForm.SelectedIndex = RawFormIndex;
            this.tbDescription.Text = Description;
            this.Refresh(); 
            this.updateTB();
        }

        private void cmbxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbxType.SelectedIndex == 1)
            {  // What should happen if the Selected index is Buying
                cmbxGroupProcess.SelectedIndex = 0;
                cmbxGroupProcess.Enabled = false;
                cmbxMachiningProcess1.SelectedIndex = 0;
                cmbxMachiningProcess1.Enabled = false;
                cmbxMachiningProcess2.SelectedIndex = 0;
                cmbxMachiningProcess2.Enabled = false;
                cmbxFinishing.SelectedIndex = 0;
                cmbxFinishing.Enabled = false;
            }
            else
            {
                cmbxGroupProcess.Enabled = true;
                cmbxMachiningProcess1.Enabled = true;
                cmbxMachiningProcess2.Enabled = true;
                cmbxFinishing.Enabled = true;
            }
            updateTB();
        }



        internal void ToggleDescriptionAlarm(bool alarm)
        {
            if (alarm)
            {
                tbDescription.BackColor = Color.Red;
            }
            else { 
                tbDescription.BackColor = Color.White; 
            }
        }
    }
}

﻿namespace npap.PartIDGenerator.frms
{
    partial class DataUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbxType = new System.Windows.Forms.ComboBox();
            this.cmbxGroupProcess = new System.Windows.Forms.ComboBox();
            this.cmbxMachiningProcess1 = new System.Windows.Forms.ComboBox();
            this.cmbxMachiningProcess2 = new System.Windows.Forms.ComboBox();
            this.cmbxFinishing = new System.Windows.Forms.ComboBox();
            this.cmbxRawForm = new System.Windows.Forms.ComboBox();
            this.tbSortingCode = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.lblMajorProcess = new System.Windows.Forms.Label();
            this.lblMachnining1 = new System.Windows.Forms.Label();
            this.lblMachnining2 = new System.Windows.Forms.Label();
            this.lblFinishing = new System.Windows.Forms.Label();
            this.lblRawForm = new System.Windows.Forms.Label();
            this.lblDesciption = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.lblComments = new System.Windows.Forms.Label();
            this.tbComments = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmbxType
            // 
            this.cmbxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxType.FormattingEnabled = true;
            this.cmbxType.Location = new System.Drawing.Point(7, 23);
            this.cmbxType.Name = "cmbxType";
            this.cmbxType.Size = new System.Drawing.Size(121, 21);
            this.cmbxType.TabIndex = 0;
            this.cmbxType.SelectedIndexChanged += new System.EventHandler(this.cmbxType_SelectedIndexChanged);
            // 
            // cmbxGroupProcess
            // 
            this.cmbxGroupProcess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxGroupProcess.FormattingEnabled = true;
            this.cmbxGroupProcess.Location = new System.Drawing.Point(134, 23);
            this.cmbxGroupProcess.Name = "cmbxGroupProcess";
            this.cmbxGroupProcess.Size = new System.Drawing.Size(121, 21);
            this.cmbxGroupProcess.TabIndex = 1;
            this.cmbxGroupProcess.SelectedIndexChanged += new System.EventHandler(this.updateTB);
            // 
            // cmbxMachiningProcess1
            // 
            this.cmbxMachiningProcess1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxMachiningProcess1.FormattingEnabled = true;
            this.cmbxMachiningProcess1.Location = new System.Drawing.Point(7, 63);
            this.cmbxMachiningProcess1.Name = "cmbxMachiningProcess1";
            this.cmbxMachiningProcess1.Size = new System.Drawing.Size(121, 21);
            this.cmbxMachiningProcess1.TabIndex = 2;
            this.cmbxMachiningProcess1.SelectedIndexChanged += new System.EventHandler(this.updateTB);
            // 
            // cmbxMachiningProcess2
            // 
            this.cmbxMachiningProcess2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxMachiningProcess2.FormattingEnabled = true;
            this.cmbxMachiningProcess2.Location = new System.Drawing.Point(134, 63);
            this.cmbxMachiningProcess2.Name = "cmbxMachiningProcess2";
            this.cmbxMachiningProcess2.Size = new System.Drawing.Size(121, 21);
            this.cmbxMachiningProcess2.TabIndex = 3;
            this.cmbxMachiningProcess2.SelectedIndexChanged += new System.EventHandler(this.updateTB);
            // 
            // cmbxFinishing
            // 
            this.cmbxFinishing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxFinishing.FormattingEnabled = true;
            this.cmbxFinishing.Location = new System.Drawing.Point(7, 103);
            this.cmbxFinishing.Name = "cmbxFinishing";
            this.cmbxFinishing.Size = new System.Drawing.Size(121, 21);
            this.cmbxFinishing.TabIndex = 4;
            this.cmbxFinishing.SelectedIndexChanged += new System.EventHandler(this.updateTB);
            // 
            // cmbxRawForm
            // 
            this.cmbxRawForm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxRawForm.FormattingEnabled = true;
            this.cmbxRawForm.Location = new System.Drawing.Point(134, 103);
            this.cmbxRawForm.Name = "cmbxRawForm";
            this.cmbxRawForm.Size = new System.Drawing.Size(121, 21);
            this.cmbxRawForm.TabIndex = 4;
            this.cmbxRawForm.SelectedIndexChanged += new System.EventHandler(this.updateTB);
            // 
            // tbSortingCode
            // 
            this.tbSortingCode.Location = new System.Drawing.Point(17, 150);
            this.tbSortingCode.Name = "tbSortingCode";
            this.tbSortingCode.ReadOnly = true;
            this.tbSortingCode.Size = new System.Drawing.Size(248, 20);
            this.tbSortingCode.TabIndex = 5;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(4, 7);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(31, 13);
            this.lblType.TabIndex = 6;
            this.lblType.Text = "Type";
            // 
            // lblMajorProcess
            // 
            this.lblMajorProcess.AutoSize = true;
            this.lblMajorProcess.Location = new System.Drawing.Point(140, 7);
            this.lblMajorProcess.Name = "lblMajorProcess";
            this.lblMajorProcess.Size = new System.Drawing.Size(74, 13);
            this.lblMajorProcess.TabIndex = 7;
            this.lblMajorProcess.Text = "Major Process";
            // 
            // lblMachnining1
            // 
            this.lblMachnining1.AutoSize = true;
            this.lblMachnining1.Location = new System.Drawing.Point(4, 47);
            this.lblMachnining1.Name = "lblMachnining1";
            this.lblMachnining1.Size = new System.Drawing.Size(65, 13);
            this.lblMachnining1.TabIndex = 7;
            this.lblMachnining1.Text = "Machining 1";
            // 
            // lblMachnining2
            // 
            this.lblMachnining2.AutoSize = true;
            this.lblMachnining2.Location = new System.Drawing.Point(134, 47);
            this.lblMachnining2.Name = "lblMachnining2";
            this.lblMachnining2.Size = new System.Drawing.Size(65, 13);
            this.lblMachnining2.TabIndex = 7;
            this.lblMachnining2.Text = "Machining 2";
            // 
            // lblFinishing
            // 
            this.lblFinishing.AutoSize = true;
            this.lblFinishing.Location = new System.Drawing.Point(4, 87);
            this.lblFinishing.Name = "lblFinishing";
            this.lblFinishing.Size = new System.Drawing.Size(48, 13);
            this.lblFinishing.TabIndex = 7;
            this.lblFinishing.Text = "Finishing";
            // 
            // lblRawForm
            // 
            this.lblRawForm.AutoSize = true;
            this.lblRawForm.Location = new System.Drawing.Point(131, 87);
            this.lblRawForm.Name = "lblRawForm";
            this.lblRawForm.Size = new System.Drawing.Size(102, 13);
            this.lblRawForm.TabIndex = 7;
            this.lblRawForm.Text = "Raw Form/Category";
            // 
            // lblDesciption
            // 
            this.lblDesciption.AutoSize = true;
            this.lblDesciption.Location = new System.Drawing.Point(269, 7);
            this.lblDesciption.Name = "lblDesciption";
            this.lblDesciption.Size = new System.Drawing.Size(60, 13);
            this.lblDesciption.TabIndex = 7;
            this.lblDesciption.Text = "Description";
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(267, 23);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(235, 20);
            this.tbDescription.TabIndex = 8;
            // 
            // lblComments
            // 
            this.lblComments.AutoSize = true;
            this.lblComments.Location = new System.Drawing.Point(264, 47);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(56, 13);
            this.lblComments.TabIndex = 7;
            this.lblComments.Text = "Comments";
            // 
            // tbComments
            // 
            this.tbComments.Location = new System.Drawing.Point(267, 63);
            this.tbComments.Multiline = true;
            this.tbComments.Name = "tbComments";
            this.tbComments.Size = new System.Drawing.Size(235, 61);
            this.tbComments.TabIndex = 9;
            // 
            // DataUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.tbComments);
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.lblDesciption);
            this.Controls.Add(this.lblRawForm);
            this.Controls.Add(this.lblFinishing);
            this.Controls.Add(this.lblMachnining2);
            this.Controls.Add(this.lblMachnining1);
            this.Controls.Add(this.lblComments);
            this.Controls.Add(this.lblMajorProcess);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.tbSortingCode);
            this.Controls.Add(this.cmbxRawForm);
            this.Controls.Add(this.cmbxFinishing);
            this.Controls.Add(this.cmbxMachiningProcess2);
            this.Controls.Add(this.cmbxMachiningProcess1);
            this.Controls.Add(this.cmbxGroupProcess);
            this.Controls.Add(this.cmbxType);
            this.Name = "DataUserControl";
            this.Size = new System.Drawing.Size(518, 132);
            this.Load += new System.EventHandler(this.DataUserControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbxType;
        private System.Windows.Forms.ComboBox cmbxGroupProcess;
        private System.Windows.Forms.ComboBox cmbxMachiningProcess1;
        private System.Windows.Forms.ComboBox cmbxMachiningProcess2;
        private System.Windows.Forms.ComboBox cmbxFinishing;
        private System.Windows.Forms.ComboBox cmbxRawForm;
        private System.Windows.Forms.TextBox tbSortingCode;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblMajorProcess;
        private System.Windows.Forms.Label lblMachnining1;
        private System.Windows.Forms.Label lblMachnining2;
        private System.Windows.Forms.Label lblFinishing;
        private System.Windows.Forms.Label lblRawForm;
        private System.Windows.Forms.Label lblDesciption;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label lblComments;
        private System.Windows.Forms.TextBox tbComments;

    }
}

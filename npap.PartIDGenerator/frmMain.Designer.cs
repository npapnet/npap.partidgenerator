﻿namespace npap.PartIDGenerator
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spltCntrMain = new System.Windows.Forms.SplitContainer();
            this.btnAddPart = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnDeletePart = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.sfdFileName = new System.Windows.Forms.SaveFileDialog();
            this.ofdMain = new System.Windows.Forms.OpenFileDialog();
            this.dataUserControl1 = new npap.PartIDGenerator.frms.DataUserControl();
            this.tbFilename = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spltCntrMain)).BeginInit();
            this.spltCntrMain.Panel1.SuspendLayout();
            this.spltCntrMain.Panel2.SuspendLayout();
            this.spltCntrMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // spltCntrMain
            // 
            this.spltCntrMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltCntrMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spltCntrMain.Location = new System.Drawing.Point(0, 0);
            this.spltCntrMain.Name = "spltCntrMain";
            this.spltCntrMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltCntrMain.Panel1
            // 
            this.spltCntrMain.Panel1.Controls.Add(this.tbFilename);
            this.spltCntrMain.Panel1.Controls.Add(this.dataUserControl1);
            this.spltCntrMain.Panel1.Controls.Add(this.btnAddPart);
            this.spltCntrMain.Panel1.Controls.Add(this.btnUpdate);
            this.spltCntrMain.Panel1.Controls.Add(this.btnSave);
            this.spltCntrMain.Panel1.Controls.Add(this.btnLoad);
            this.spltCntrMain.Panel1.Controls.Add(this.btnDeletePart);
            // 
            // spltCntrMain.Panel2
            // 
            this.spltCntrMain.Panel2.Controls.Add(this.dataGridView1);
            this.spltCntrMain.Size = new System.Drawing.Size(755, 374);
            this.spltCntrMain.SplitterDistance = 186;
            this.spltCntrMain.TabIndex = 0;
            // 
            // btnAddPart
            // 
            this.btnAddPart.Location = new System.Drawing.Point(549, 12);
            this.btnAddPart.Name = "btnAddPart";
            this.btnAddPart.Size = new System.Drawing.Size(75, 23);
            this.btnAddPart.TabIndex = 3;
            this.btnAddPart.Text = "Add Part";
            this.btnAddPart.UseVisualStyleBackColor = true;
            this.btnAddPart.Click += new System.EventHandler(this.btnAddPart_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(549, 70);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "UpdateRow";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(668, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save ";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(668, 41);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 4;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnDeletePart
            // 
            this.btnDeletePart.Location = new System.Drawing.Point(549, 41);
            this.btnDeletePart.Name = "btnDeletePart";
            this.btnDeletePart.Size = new System.Drawing.Size(75, 23);
            this.btnDeletePart.TabIndex = 5;
            this.btnDeletePart.Text = "Delete Row";
            this.btnDeletePart.UseVisualStyleBackColor = true;
            this.btnDeletePart.Click += new System.EventHandler(this.btnDeletePart_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(755, 184);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            this.dataGridView1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            // 
            // sfdFileName
            // 
            this.sfdFileName.Filter = "XML files|*.xml";
            this.sfdFileName.Title = "Select file to save";
            // 
            // ofdMain
            // 
            this.ofdMain.Filter = "XML files|*.xml";
            this.ofdMain.Title = "Select File to open";
            // 
            // dataUserControl1
            // 
            this.dataUserControl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataUserControl1.Location = new System.Drawing.Point(12, 12);
            this.dataUserControl1.Name = "dataUserControl1";
            this.dataUserControl1.Size = new System.Drawing.Size(519, 135);
            this.dataUserControl1.TabIndex = 2;
            // 
            // tbFilename
            // 
            this.tbFilename.Location = new System.Drawing.Point(13, 154);
            this.tbFilename.Name = "tbFilename";
            this.tbFilename.ReadOnly = true;
            this.tbFilename.Size = new System.Drawing.Size(518, 20);
            this.tbFilename.TabIndex = 6;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 374);
            this.Controls.Add(this.spltCntrMain);
            this.Name = "frmMain";
            this.Text = "Part ID Generator ";
            this.spltCntrMain.Panel1.ResumeLayout(false);
            this.spltCntrMain.Panel1.PerformLayout();
            this.spltCntrMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltCntrMain)).EndInit();
            this.spltCntrMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spltCntrMain;
        private frms.DataUserControl dataUserControl1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAddPart;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.SaveFileDialog sfdFileName;
        private System.Windows.Forms.OpenFileDialog ofdMain;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDeletePart;
        private System.Windows.Forms.TextBox tbFilename;
    }
}


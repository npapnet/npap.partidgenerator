﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace npap.PartIDGenerator.Model
{

    public static class GlobalConstants
    {

        public static readonly Dictionary<string, char> dictType = new Dictionary<string, char>
        {
            {"Manufacture", 'M'},
            {"Buying", 'B'},
            {"Document", 'D'},
        };
                  
        public static readonly Dictionary<string, int> dictGroupProcess = new Dictionary<string, int>
        {
             { "None", 0 },
             { "Assembly", 1 },
             { "Plasma Cut", 2 },
             { "Laser Cut", 3 },
             { "Punching Cut", 4 },
             { "Hydro Cut ", 5 },
             { "Machining", 6 },
             { "Molding", 7 },
             { "Plastic", 8 },
             { "Casting", 9 }
        };


        public static readonly Dictionary<string, int> dictMachiningProcess = new Dictionary<string, int>        {
            { "None", 0 },     
            { "Bend", 1 },
             { "Engrave", 2 },
             { "Machining", 3 },
             { "Welding", 4 },
        };

        public static readonly Dictionary<string, int> dictFinishingProcess = new Dictionary<string, int>        
        {
            { "None", 0 },
            { "HotDip Galvanize", 1 },
            { "Electrolytic Galvanize", 2 },
            { "Painting", 3 }
        };
        public static readonly Dictionary<string, int> dictRawType = new Dictionary<string, int>
        {
             { "None", 0 },
             { "Plate", 1 },
             { "Sheet metal", 2 },
             { "Round bar", 3 },
             { "Hexagon bar", 4 },
             { "Structural ", 5 },
             { "Fasteners", 6 },
             { "Hydraulics", 7 },
             { "Plastic", 8 },
             { "PowerTransmission", 9 }
        };
    }
}

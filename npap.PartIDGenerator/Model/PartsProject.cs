﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using npap.PartIDGenerator.Model;
using npap.PartIDGenerator.frms;
using System.Xml.Serialization;

namespace npap.PartIDGenerator.Model
{
    [XmlRoot("Project")]
    [XmlInclude(typeof(Part))]
    public class PartsProject
    {
        [XmlArray("Parts")]
        [XmlArrayItem("Part")]
        public List<Part> Parts { get; set; }
        [XmlIgnoreAttribute]
        internal int PIDCounter=0;

        public PartsProject()
        {
            this.Parts = new List<Part>();
            PIDCounter = 0;
        }

        public void AddPart(DataUserControl myDUC)
        {
            this.PIDCounter += 1;
            this.Parts.Add(new Part(PIDCounter, myDUC.getSortCode(), myDUC.getDescription()));
            this.Parts[this.Parts.Count-1].PartIDDataUserControl.UpdateDUC( myDUC);
        }


        internal void DeletePart(DataUserControl dataUserControl1, int p)
        {
            this.Parts.RemoveAt(p-1);

        }

        internal void UpdatePart(int ListIndex, DataUserControl dataUserControl)
        {
            this.Parts[ListIndex].PartIDDataUserControl.UpdateDUC(dataUserControl);
            //TODO sort TYPE
            this.Parts[ListIndex].GroupProcess = dataUserControl.getGroupProcess();
            this.Parts[ListIndex].Machining1 = dataUserControl.getMachiningProcess1();
            this.Parts[ListIndex].Machining2 = dataUserControl.getMachiningProcess2();
            this.Parts[ListIndex].Finishing = dataUserControl.getFinishing();
            this.Parts[ListIndex].RawType = dataUserControl.getRawForm();
            this.Parts[ListIndex].Description = dataUserControl.getDescription();
        }
    }
}

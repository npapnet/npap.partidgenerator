﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using npap.PartIDGenerator.Model;
using npap.PartIDGenerator.frms;
using System.ComponentModel;
using System.Xml.Serialization;
namespace npap.PartIDGenerator.Model
{
    public class Part
    {
        #region Variable Declaration*****************
        [DisplayName("Part ID")]
        public int _PartIDI { get; set; }
        [DisplayName("Document Type")]
        //TODO I might need to correct this. This could be a complete KEY from Global Constants
        public string Type { get; set; }
        [DisplayName("Group Process")]
        public string GroupProcess { get; set; }
        [DisplayName("Machining 1st")]
        public string Machining1 { get; set; }
        [DisplayName("Machining 2nd")]
        public string Machining2 { get; set; }
        public string Finishing { get; set; }
        public string RawType { get; set; }
        public string Description { get; set; }
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public DataUserControl PartIDDataUserControl { get; set; }
        private string _SortCode { get; set; }
        
        #endregion


        #region Constructor/Initialisation ***

        public Part()
        {

        }

        public Part(int IncrementalID, string Type, int GroupProcess, int Machining1, int Machining2, int Finishing, int RawType, string Description)
        {
            this._PartIDI = IncrementalID;
            this.Type = Type;
            this.GroupProcess = GlobalConstants.dictGroupProcess.Keys.ElementAt(GroupProcess).ToString();
            this.Machining1 = GlobalConstants.dictMachiningProcess.Keys.ElementAt(Machining1).ToString();
            this.Machining2 = GlobalConstants.dictMachiningProcess.Keys.ElementAt(Machining2).ToString(); ;
            this.Finishing = GlobalConstants.dictFinishingProcess.Keys.ElementAt(Finishing).ToString(); ;
            this.RawType = GlobalConstants.dictRawType.Keys.ElementAt(RawType).ToString();
            this.Description = Description;
            PartIDDataUserControl = new DataUserControl();
        }

        public Part(int IncrementalID, string SortCode, string Description)
            : this(IncrementalID, SortCode.Substring(0, 1),
                int.Parse(SortCode.Substring(1, 2)),
                int.Parse(SortCode.Substring(3, 2)),
                int.Parse(SortCode.Substring(5, 2)),
                int.Parse(SortCode.Substring(7, 2)),
                int.Parse(SortCode.Substring(9, 2)),
                Description
                )
        {
        }

        public Part(int IncrementalID, DataUserControl thisDUC)
            : this(IncrementalID, thisDUC.getSortCode(), thisDUC.getDescription())
        {
        } 
        #endregion

        public void UsePropertiesForDUC()
        {
            int _docType = getDocumentTypeIndex(this.Type);
            int _grpInd= GlobalConstants.dictGroupProcess[this.GroupProcess];
            int _Machining1= GlobalConstants.dictMachiningProcess[this.Machining1];
            int _Machining2= GlobalConstants.dictMachiningProcess[this.Machining2];
            int _Finishing= GlobalConstants.dictFinishingProcess[this.Finishing];
            int _rawType= GlobalConstants.dictRawType[this.RawType];
            PartIDDataUserControl = new DataUserControl();
            PartIDDataUserControl.UpdateDUC(_docType, _grpInd, _Machining1, _Machining2, _Finishing, _rawType, this.Description);
        }
        //TODO move this to Global constants
        private static int getDocumentTypeIndex(string DocTypeString)
        {
            int _docType;
            switch (DocTypeString.ToUpper().ToCharArray()[0])
            {
                case 'M':
                    {
                        _docType = 0;
                        break;
                    }
                case 'B':
                    _docType = 1;
                    break;
                case 'D':
                    _docType = 2;
                    break;
                default:
                    _docType = 0;
                    break;
            }
            return _docType;
        }



        internal string getFilename()
        {
            return this.PartIDDataUserControl.getSortCode() + "-" + this._PartIDI.ToString("00000") + "-" + this.Description;
        }
    }
}

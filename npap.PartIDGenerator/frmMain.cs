﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using npap.PartIDGenerator.Model;
using npap.PartIDGenerator.frms;


namespace npap.PartIDGenerator
{
    public partial class frmMain : Form
    {
        #region Variable Declaration *************************************
        PartsProject currentProject;
        BindingList<Part> source = new BindingList<Part>();
        FileStream fs;
        System.Xml.Serialization.XmlSerializer x;
        
        #endregion


        #region Constructor/ Initialisation***********************
        public frmMain()
        {
            InitializeComponent();
            currentProject = new PartsProject();
            source = new BindingList<Part>(currentProject.Parts);
            dataGridView1.DataSource = source;
            x = new System.Xml.Serialization.XmlSerializer(typeof(PartsProject));
        }

        #endregion



        #region Data Grid Events************************************

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                dataUserControl1.UpdateDUC(currentProject.Parts[dataGridView1.CurrentCell.RowIndex].PartIDDataUserControl);
                tbFilename.Text = currentProject.Parts[dataGridView1.CurrentCell.RowIndex].getFilename();
            }
            catch (Exception)
            { }
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                dataUserControl1.UpdateDUC(currentProject.Parts[dataGridView1.CurrentCell.RowIndex].PartIDDataUserControl);
                InverseButtons();

            }
            catch (Exception)
            { }

        }

        private void InverseButtons()
        {
            btnAddPart.Visible = !btnAddPart.Visible;
            btnDeletePart.Visible = !btnDeletePart.Visible;
            btnUpdate.Visible = !btnUpdate.Visible;
            btnLoad.Visible = !btnLoad.Visible;
            btnSave.Visible = !btnSave.Visible;
            if (btnUpdate.Visible)
            {
                this.Text = "Part ID Generator - Update Mode";
                this.BackColor = Color.DeepSkyBlue;
            }
            else
            {
                 this.Text = "Part ID Generator";
                 this.BackColor = SystemColors.Control;
            }

        }
        #endregion

        #region Single Part Events *************************
        private void btnAddPart_Click(object sender, EventArgs e)
        {
            if (dataUserControl1.getDescription() == "")
            {
                MessageBox.Show("Please Provide Short Description");
                dataUserControl1.ToggleDescriptionAlarm(true);
            }
            else
            {
                dataUserControl1.ToggleDescriptionAlarm(false);
                currentProject.AddPart(dataUserControl1);
                dataGridView1.DataSource = new BindingList<Part>(currentProject.Parts);
                dataGridView1.Rows[currentProject.Parts.Count - 1].Selected = true;
                dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.SelectedRows[0].Index;
            }
        }

        private void btnDeletePart_Click(object sender, EventArgs e)
        {
            currentProject.DeletePart(dataUserControl1, dataGridView1.CurrentCell.RowIndex);
            dataGridView1.DataSource = new BindingList<Part>(currentProject.Parts);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            currentProject.UpdatePart(dataGridView1.CurrentCell.RowIndex, dataUserControl1);
            dataGridView1.DataSource = new BindingList<Part>(currentProject.Parts);
            InverseButtons();
        }
        #endregion

        #region Part List Project ***************************
        /// <summary>
        /// Save to file function
        /// Serialises data and writes it in an XML file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            sfdFileName.ShowDialog();
            if (sfdFileName.FileName != "")
            {
                fs = new FileStream(sfdFileName.FileName, FileMode.Create);
                x.Serialize(fs, currentProject);
                fs.Close();
            }
        }

        /// <summary>
        /// Loads datas from File
        /// Closes file
        /// Parses data
        /// Updates Part DUC
        /// Update PIDcounter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoad_Click(object sender, EventArgs e)
        {
            //Initialise
            currentProject.Parts.Clear();
            currentProject.PIDCounter = 0;
            //Load File
            ofdMain.ShowDialog();
            fs = new FileStream(ofdMain.FileName, FileMode.Open);
            currentProject = (PartsProject)x.Deserialize(fs);
            fs.Close();

            foreach (Part tempPart in currentProject.Parts)
            {
                tempPart.UsePropertiesForDUC();
            }
            currentProject.PIDCounter = currentProject.Parts[currentProject.Parts.Count - 1]._PartIDI;
            dataGridView1.DataSource = new BindingList<Part>(currentProject.Parts);
        }
        
        #endregion




    }
}

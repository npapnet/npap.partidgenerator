﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using npap.PartIDGenerator.Model;

namespace npap.PartIDGenerator.Test
{
    [TestClass]
    public class PartTest
    {
        [TestMethod]
        public void Test()
        {
            string t = "Manufactured";
            char c = 'M';
            Assert.AreEqual(t.ToCharArray()[0], c);

            string[] sAr=new string[3];
            GlobalConstants.dictType.Keys.CopyTo(sAr, 0);
            Assert.AreEqual( "Manufacture",sAr[0]);
            Assert.AreEqual(c, sAr[0].ToCharArray()[0]);
            Assert.AreEqual('B',sAr[1].ToCharArray()[0]);
            Assert.AreEqual('D',sAr[2].ToCharArray()[0]);
            
        }
    }
}
